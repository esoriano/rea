# REA

Inject short reads and break stuff.

## Do you know how to use *read(2)*?

The *read(2)* system call is pretty simple. Nevertheless, a lot of programmers don't use it correctly.

A read of N bytes may return a number M of bytes, being 0<M<=N. if M<N, there is
a *short read*; it's not an error (read returns -1 on error) or EOF (read returns 0 in this case). Short reads are common when using synthetic files, pipes, consoles, etc. On the other hand, conventional filesystems (like EXT3) cause few short reads. Anyway, there can be short reads. As the manual page explains:

```
On success, the number of bytes read is returned (zero indicates end
of file), and the file position is advanced by this number.  It is not
an error if this number is smaller than the  number of bytes requested;
this may happen for example because fewer bytes are actually available
right now (maybe because we were close to end-of-file, or because we
are reading from  a  pipe, or from a terminal), or because read() was
interrupted by a signal...
```

Rea is a library that hooks the libc to force short reads. It can be used
to test your programs and find bugs in existing software.

Rea uses a common PRNG (*random(3)*) in order to be deterministic.

## Environment

You can set the following environment variables to change some parameters:

`REA_ALL` if it's set and the value is "yes", rea will ignore the filesystem type and short reads will be forced for all filesystems (note that this will break a lot of things, e.g. `/dev/`, `/proc/`, etc.). By default, rea only interferes with EXT filesystems.

`REA_PROB` defines the probability (%) P to inject a short read (0<P<=100).  By default, P is 50.

`REA_SEED` sets the seed. By default, the seed is 13.

## Usage

Define `LD_PRELOAD`:

`export LD_PRELOAD=$(pwd)/librea.so`

Maybe you will need to force the load of `dl` too:

`export LD_PRELOAD=/usr/lib/libdl.so:$(pwd)/librea.so`

Run the victim.

## Test

To run a basic test:

```
$> make all
$> export LD_PRELOAD=$(pwd)/librea.so
$> cd test
$> make run
test -d /tmp/reatest || mkdir /tmp/reatest
cp /usr/bin/python3 /tmp/reatest/basic-test-in
./test-basic /tmp/reatest/basic-test-in > /tmp/reatest/basic-test-out
short read, count: 2687 requested: 8192
short read, count: 4898 requested: 8192
short read, count: 1150 requested: 8192
...
short read, count: 7348 requested: 8192
short read, count: 8002 requested: 8192
short read, count: 4900 requested: 8192
short read, count: 628 requested: 8192
short read, count: 843 requested: 8192
Total reads: 907, short reads: 477
ls -l /tmp/reatest/basic-test-out
-rw-rw-r-- 1 esoriano esoriano 5490488 Dec 23 15:12 /tmp/reatest/basic-test-out
cmp /tmp/reatest/basic-test-in /tmp/reatest/basic-test-out
$>
```
