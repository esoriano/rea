#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <err.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

enum{
	Bsz = 1024*8,
};

int
main(int argc, char *argv[])
{
	int fd;
	char b[Bsz];
	int nr;
	int nshort = 0;
	int total = 0;

	if(argc != 2) {
		errx(EXIT_FAILURE, "usage: ./test-basic path");
	}
	fd = open(argv[1], O_RDONLY);
	if(fd < 0){
		err(EXIT_FAILURE, "open failed");
	}

	while((nr = read(fd, b, Bsz)) > 0){
		if(nr < Bsz) {
			fprintf(stderr, "short read, count: %d "
						"requested: %d\n", nr, Bsz);
			nshort++;
		}
		if(write(1, b, nr) != nr){
			err(EXIT_FAILURE, "write failed");
		}
		total++;
	}
	if(nr < 0){
		err(EXIT_FAILURE, "read failed");
	}
	close(fd);
	fprintf(stderr, "Total reads: %d, short reads: %d\n", total, nshort);
	exit(EXIT_SUCCESS);
}
