CC = gcc
L= -ldl
CFLAGS = -g -Wall -Wshadow -fPIC -o -I.
SRCS = $(wildcard *.c)
OBJS = $(SRCS:.c=.o)
TOCLEAN = librea.so $(OBJS)

all: librea.so
	$(MAKE) -C test all

librea.so: rea.o
	$(CC) -shared -fPIC -Wl,-soname -Wl,$@  -o $@ $< $L

%.o: %.c
	$(CC) -c $(CFLAGS) $(CPPFLAGS) $< -o $@

clean:
	rm -f $(TOCLEAN)
	$(MAKE) -C test clean

.PHONY: all clean
