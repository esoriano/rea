/*
Copyright (C) 2021  Enrique Soriano <esoriano@gsyc.urjc.es>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses
*/

#define _GNU_SOURCE

#include <stdio.h>
#include <dlfcn.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <err.h>
#include <sys/vfs.h>

enum{
	Statesz = 256,
	Defseed = 13,
	Defp = 50, // default prob (%) to change count

	/* from statfs(2) */
	Ext_super_magic		=   0x137d,
	Ext2_old_super_magic	=   0xef51,
	Extmodern_super_magic	=   0xef53,
	Ramfs_magic		=   0x858458f6,
	Tmpfs_magic		=   0x01021994, //also /dev
};

void
changecount(size_t *c)
{
	static int p = -1;
	char *v;
	long r;

	if(p == -1){
		p = Defp;
		v = getenv("REA_PROB");
		if(v != NULL){
			p = atoi(v);
			if(p <= 0 || p > 100){
				p = Defp;
			}
		}
	}
	r = random();
	if(r % 100 < p){
		*c = (r % *c) + 1;
	}
}

/*
 * By default, we only interfere with EXT filesystems
 */
int
isfsok(int fd)
{
	struct statfs b;

	if(fstatfs(fd, &b) < 0){
		return 0;
	}
	switch(b.f_type){
	case Extmodern_super_magic:
	case Ext2_old_super_magic:
	case Ext_super_magic:
		return b.f_type;
	}
	return 0;
}

ssize_t
read(int fd, void *buf, size_t count)
{
	static char state[Statesz];
	static typeof(read) *legit = NULL;
	static int ignorefs = 0;
	unsigned int seed;
	char *v;
	char *oldstate;

	if(legit == NULL){
		legit = dlsym(RTLD_NEXT, "read");
		if(legit == NULL){
			errx(EXIT_FAILURE, "rea can't resolve read");
		}
		v = getenv("REA_SEED");
		seed = Defseed;
		if(v != NULL){
			seed = (unsigned int) atoi(v);
		}
		if(initstate(seed, state, Statesz) == NULL){
			err(EXIT_FAILURE, "rea can't init state");
		}
		v = getenv("REA_ALL");
		ignorefs = v != NULL && strcmp(v, "yes") == 0;
		fprintf(stderr, "rea is enabled\n");
	}
	if(ignorefs || isfsok(fd)){
		oldstate = setstate(state);
		changecount(&count);
		if(oldstate != NULL){
			if(setstate(oldstate) == NULL){
				err(EXIT_FAILURE, "rea can't restore state");
			}
		}
	}
	return legit(fd, buf, count);
}
